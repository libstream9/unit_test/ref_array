#include <stream9/container/ref_array.hpp>

#include <vector>

#include <boost/test/unit_test.hpp>

namespace testing {

using stream9::container::ref_array;

namespace json { using namespace stream9::json; }

BOOST_AUTO_TEST_SUITE(ref_array_)

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        ref_array<int> a;
    }

    BOOST_AUTO_TEST_CASE(range_constructor_)
    {
        std::vector<int> v { 1, 2, 3 };
        ref_array ref { v };

        json::array expected { 1, 2, 3 };
        BOOST_TEST(json::value_from(ref) == expected);
    }

    BOOST_AUTO_TEST_CASE(initializer_list_)
    {
        int i1 = 1, i2 = 2;

        ref_array ref { std::ref(i1), std::ref(i2) };

        json::array expected { 1, 2 };
        BOOST_TEST(json::value_from(ref) == expected);
    }

    BOOST_AUTO_TEST_CASE(at_)
    {
        std::vector<int> v { 1, 2, 3 };
        ref_array ref { v };

        BOOST_TEST(ref.at(0) == 1);
        BOOST_TEST(ref.at(1) == 2);
        BOOST_TEST(ref.at(2) == 3);

        BOOST_CHECK_EXCEPTION(ref.at(3),
            stream9::error,
            [&](auto&& e) {
                using enum ref_array<int>::errc;
                BOOST_TEST(e.why() == index_is_out_of_range);
                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(push_back_)
    {
        ref_array<int> ref;
        int a = 1;

        ref.push_back(a);
        ref.push_back(a);

        json::array expected { 1, 1, };
        BOOST_TEST(json::value_from(ref) == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_1_)
    {
        ref_array<int> ref;
        int a1 = 1;
        int a2 = 2;

        ref.insert(ref.end(), a1);
        ref.insert(ref.begin(), a2);

        json::array expected { 2, 1, };
        BOOST_TEST(json::value_from(ref) == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_2_)
    {
        std::vector<int> v { 1, 2, 3 };
        ref_array<int> ref;

        ref.insert(ref.end(), v.begin(), v.end());

        json::array expected { 1, 2, 3, };
        BOOST_TEST(json::value_from(ref) == expected);
    }

    BOOST_AUTO_TEST_CASE(erase_1_)
    {
        int a = 1;
        ref_array<int> ref { std::ref(a) };

        ref.erase(ref.begin());

        BOOST_TEST(ref.empty());
    }

    BOOST_AUTO_TEST_CASE(erase_2_)
    {
        std::vector<int> v { 1, 2, 3 };
        ref_array ref { v };

        ref.erase(ref.begin(), ref.begin() + 2);

        json::array expected { 3 };
        BOOST_TEST(json::value_from(ref) == expected);
    }

    BOOST_AUTO_TEST_CASE(pop_back_)
    {
        std::vector<int> v { 1, 2, 3 };
        ref_array ref { v };

        ref.pop_back();

        json::array expected { 1, 2 };
        BOOST_TEST(json::value_from(ref) == expected);
    }

    BOOST_AUTO_TEST_CASE(reserve_)
    {
        ref_array<int> ref;

        BOOST_TEST(ref.capacity() == 0);

        ref.reserve(10);

        BOOST_TEST(ref.capacity() >= 10);
    }

BOOST_AUTO_TEST_SUITE_END() // ref_array_

} // namespace testing
